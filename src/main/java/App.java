import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class App extends Application {

    @Override
    public void start(Stage stage) {
        var label = new Label("Bienvenido a la plantilla Simple");
        var scene = new Scene(new StackPane(label), 640, 480);
        stage.setScene(scene);
        
        stage.setOnCloseRequest(windowEvent -> {
            var alert = new Alert(Alert.AlertType.CONFIRMATION, "¿Estás seguro de que deseas salir?");
            var result = alert.showAndWait();
            if(result.isPresent() && result.get() == ButtonType.OK){
                Platform.exit();
            } else {
                windowEvent.consume();
            }
        });
        
        stage.show();
        
    }

    @Override
    public void init() throws Exception {
        super.init(); //To change body of generated methods, choose Tools | Templates.
    }

    public static void main(String[] args) {
        launch();
    }

}